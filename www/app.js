var TODO = angular.module("TODO", [
    "ui.router",
    "mobile-angular-ui",
    "btford.socket-io",
    "ngCookies",
    "ngAnimate",
    "ngTouch",
    "swipe",
    "angularMoment",
    "angular-datepicker",
    "pasvaz.bindonce"
]);

(function() {
    
    TODO.constant("appConstants", {

		appName: {
            name: "TODO"
        },
        socket: {
			url: "http://128.199.164.19:3000",
			/*
             * For multiple namespaces, such as:
             *      http://128.199.164.19:3000/nextaction
             *      http://128.199.164.19:3000/todo
             */
			prefix: ""
		},
        userDataFirstLoadTag: true,
        projectDataFirstLoadTag: true,
        taskDataFirstLoadTag: true,
        taskStore: {
            /*
             * Note: the key should comply with this rule:
             *      keyPrefix + keySeparator +
             *      username + keySeparator + 
             *      laneName + keySeparator + 
             *      task.dueDate + keySeparator + 
             *      task.status + keySeparator +
             *      task.uuid
             */
            keyPrefix: "@obs$",
            keySeparator: "|"
        },
        userStore: {
            tokenKey: "$$$token",
            userKey: "$$$user"
        },
        projectStore: {
            keyPrefix: "$projectList@",
            keySeparator: "|",
            valueSeparator: "|"
        }
	});

    TODO.config(["$stateProvider", "$urlRouterProvider",
        function($stateProvider, $urlRouterProvider) {
        
            $urlRouterProvider.otherwise('/');
        
            $stateProvider
                .state("login", {
                    url: "/",
                    views: {
                        "con": {
                            templateUrl: "templates/login.tpl.html",
                            controller: "LoginCtrl"
                        }
                    }
                })
                .state("personalBoard", {
                    url: "/pb",
                    views: {
                        "con": {
                            templateUrl: "templates/nav.tpl.html",
                            controller: "PersonalBoardCtrl"
                        }
                     }
                })
                .state("calender", {
                    url: "/c",
                    views: {
                        con: {
                            templateUrl: "templates/calender.tpl.html",
                            controller: "CalenderCtrl"
                        }
                    }
                }).
                state("taskDetail", {
                    url: "/td",
                    views: {
                        con: {
                            templateUrl: "templates/task.details.tpl.html",
                            controller: "TaskDetailCtrl"
                        }
                    }
                }).state("subTask", {
                    url: "/st",
                    views: {
                        con: {
                            templateUrl: "templates/sub.task.tpl.html",
                            controller: "SubTaskCtrl"
                        }
                    }
                }).state("members", {
                    url: "/ms",
                    views: {
                        con: {
                            templateUrl: "templates/members.tpl.html",
                            controller: "MemberCtrl"
                        }
                    }
                }).state("notifications", {
                    url: "/ns",
                    views: {
                        con: {
                            templateUrl: "templates/notification.tpl.html",
                            controller: "NotificationCtrl"
                        }
                    }
                }).state("twoLaneTasksView", {
                    url: "/ttv",
                    views: {
                        con: {
                            templateUrl: "templates/no.nav.two.lane.task.summary.tpl.html",
                            controller: "TwoLaneTaskSummaryCtrl"
                        }
                    }
                })
        }
    ]);
    
})();