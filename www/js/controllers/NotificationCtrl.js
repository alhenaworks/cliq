(function() {
    TODO.controller("NotificationCtrl", [
        "$scope", "TaskService",
        function($scope, TaskService) {

        	$scope.tasksInToday = TaskService.getTaskListForToday();

        	$scope.allTasksAmount = TaskService.tasksByLane["TO DO"].length;

        	$scope.missedTasksArray = TaskService.getMissedTaskList();

        	$scope.notifConentTpl = {
        		url: "templates/notif.tasks.for.today.tpl.html"
        	};

        	$scope.activeItemTag = "Today";

        	$scope.switchNotifList = function switchNotifList(name) {

        		$scope.activeItemTag = name;

        		switch (name) {
        			case "Today":
        				$scope.notifConentTpl.url = "templates/notif.tasks.for.today.tpl.html";
        				break;
        			case "All":
        				$scope.notifConentTpl.url = "templates/notif.all.tpl.html";
        				break;
        			case "Missed":
        				$scope.notifConentTpl.url = "templates/notif.missed.tpl.html";
        				break;
        		}
        	};
        
        }       
    ]);
})();