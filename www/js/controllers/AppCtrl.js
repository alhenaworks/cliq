(function() {

	TODO.controller("AppCtrl", [
		"$scope", "$location", "UserService",
		function($scope, $location, UserService) {

			/*
			 * Loading the welcome picture(maybe a pattern password authentification page is better)
			 */
			document.addEventListener('deviceready', function () {  
  				navigator.splashscreen.show();
			}, false);

			$scope.redirectTo = function redirectTo(url) {
                $location.url(url);
            };

            $scope.logout = function logout() {
            	UserService.logout();
            }

		}
		
	]);

})();