Array.prototype.isContain = function(value) {
	for (var i = 0; i < this.length; i++) {
		if (this[i] == value) {
			return true;
		}
	}
	return false;
}

String.prototype.isJSON = function() {
	if (typeof(this) != "undefined") {
		try {
			JSON.parse(this);
		} catch (e) {
			return false;
		}
	} else {
		return false;
	}

	return false;
}