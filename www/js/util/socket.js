(function() {

    TODO.factory("socket", [
        "socketFactory", "appConstants",
        function(socketFactory, appConstants) {

            var socket = {

                isConnected: false,

                connect: function connect(token) {
                    var self = this;
                    var url = appConstants.socket.url || "localhost";
                    var prefix = appConstants.socket.prefix;

                    console.log("Connecting socket to " + url);

                    var ioSocket = io.connect(url, {
                        forceNew: true,
                        query: "token=" + token
                    });

                    ioSocket.on("connect", function(){
                        console.log("Socket connection is successful");
                        self.isConnected = true;
                    });

                    var socketInstance = socketFactory({
                        ioSocket: ioSocket,
                        prefix: prefix
                    });

                    // mixin
                    jsface.extend(this, socketInstance);
                }

            };

            return socket;
        }
    ]);

})();