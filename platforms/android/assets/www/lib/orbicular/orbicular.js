/**
*    CoTag Orbicular
*    A more or less pure CSS, circular, progress bar
*    
*   Copyright (c) 2014 CoTag Media.
*    
*    @author     Stephen von Takach <steve@cotag.me>
*    @copyright  2014 cotag.me
* 
*     
*     References:
*        * http://fromanegg.com/post/41302147556/100-pure-css-radial-progress-bar
*        * https://medium.com/@andsens/radial-progress-indicator-using-css-a917b80c43f9
*
**/


(function (angular) {
    'use strict';

    angular.module('Orbicular', []).

        // isolated circular progress bar
        

}(this.angular));
