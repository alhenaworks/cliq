(function() {

	TODO.service("SyncServerService", [
		"socket", "db", "$rootScope", "appConstants",
		function(socket, db, $rootScope, appConstants) {

            this.retrieveTaskData = function retrieveTaskData() {
                socket.emit("system:personal_view", {action: "retrieve"});  
            };

			// Note: This method only can be invoked once
			this.syncTaskDataFromServer = function syncTaskDataFromServer(username) {

				socket.on("system:personal_view", function(data) {

                    $rootScope.$broadcast("personalTasks:update", data.personal_view);

                    if (data.personal_view.length != 0) {
    
                        console.log(data);
    
                        var currentUserToken = db.getItem(appConstants.userStore.tokenKey);
                        var currentUser = db.getItem(appConstants.userStore.userKey);
                        var projectList =
                            db.getItem(appConstants.projectStore.keyPrefix + appConstants.projectStore.keySeparator + username);
    
                        db.clear();
    
                        db.setItem(appConstants.userStore.tokenKey, currentUserToken);
                        db.setItem(appConstants.userStore.userKey, currentUser); 
                        db.setItem(appConstants.projectStore.key, projectList);
        
                        angular.forEach(data.personal_view, function(personalTask) {
        
                            db.setItem(
                                appConstants.taskStore.keyPrefix + appConstants.taskStore.keySeparator
                                + username + appConstants.taskStore.keySeparator
                                + personalTask.data.lane + appConstants.taskStore.keySeparator
                                + personalTask.data.dueDate + appConstants.taskStore.keySeparator
                                + personalTask.status + appConstants.taskStore.keySeparator
                                + personalTask._id, JSON.stringify(personalTask));                        
                        });

                    }
    
                });
			};



            this.retrieveProjectData = function retirieveProjectData() {
                socket.emit("data:fetch", {type: "projects"});
            };

            // Note: This method only can be invoked once
            this.syncProjectDataFromServer = function syncProjectDataFromServer(username) {
                socket.on("data:fetch", function(data) {

                    $rootScope.$broadcast("projects:update", data.projects);

                    console.log(data);

                    var projectList = "";

                    for (var i = 0; i < data.projects.length; i++) {                        
                        projectList += JSON.stringify(data.projects[i]);
                        if (i != data.projects.length - 1) {
                            projectList += appConstants.projectStore.valueSeparator;
                        }
                    }

                    if (projectList.length != 0) {
                        db.setItem(appConstants.projectStore.keyPrefix + appConstants.projectStore.keySeparator + username,
                            projectList);
                    }                    

                });
            };



            this.retrieveUserData = function retrieveUserData() {
                socket.emit("user:list");
            };

            // Note: This method only can be invoked once
            this.syncUserDataFromServer = function syncUserDataFromServer() {
                socket.on("user:list", function(data) {

                    $rootScope.$broadcast("user:list", data.users);

                    console.log(data);
                    // Can not modify user list in this app, so do not need to put the user list to local storage.
                });
            };

		}
	]);

})();