(function() {

	TODO.service("DBService", [
		"db", "appConstants",
		function(db, appConstants) {

			/*
			 * Build all tasks' structure from the local storage.
			 *
			 * The data structure should be:
			 * 		{
			 * 			"lanes" : ["TO DO", "This Week", "Today", "Done", ...],
			 * 			"TODO" : [],
			 * 			"This Week" : [],
			 * 			"Today" : [],
			 * 			"Done" : [],
			 * 			"Lane1" : [],
			 * 			"Lane2" :[],
			 * 			...
			 * 		}
			 *
			 */
			this.classifyAllTasksByProject = function classifyAllTasksByProject(username) {

				var start = moment().valueOf();

				var resultObj = {
					"lanes" : ["TO DO", "This Week", "Today", "Done"],
					"TO DO" : [],
					"This Week" : [],
					"Today" : [],
					"Done" : []
				};

				var currentDate = moment();
				var thisMonday = moment().weekday(1);
				var thisSunday = moment().weekday(7);

				for (var i = 0; i < db.length; i++) {

					// true : this item is created by TO DO app.
					if (db.key(i).indexOf(appConstants.taskStore.keyPrefix) != -1) {

						var keyArray = db.key(i).split(appConstants.taskStore.keySeparator);

						if (keyArray[1] == username) {

							// this task already be assigned to one lane(project).
							if (keyArray[2].length != 0) {
	
								// if the lanes still not contain this lane, then push it into the lanes
								if (!resultObj.lanes.isContain(keyArray[2])) {
									resultObj.lanes.push(keyArray[2]);
								}
	
								if (typeof(resultObj[keyArray[2]]) != "undefined") {
									resultObj[keyArray[2]].push(JSON.parse(db.getItem(db.key(i))));								
								} else {
									resultObj[keyArray[2]] = [
										JSON.parse(db.getItem(db.key(i)))
									];
								}
							}

							if (keyArray[4] == "d") {
								resultObj["Done"].push(JSON.parse(db.getItem(db.key(i))));
							} else {
								resultObj["TO DO"].push(JSON.parse(db.getItem(db.key(i))));

								var dueDate = moment(Number(keyArray[3]));
								if (dueDate.isAfter(thisMonday) && dueDate.isBefore(thisSunday)) {
									resultObj["This Week"].push(JSON.parse(db.getItem(db.key(i))));
									if (dueDate.isSame(currentDate, "day")) {
										resultObj["Today"].push(JSON.parse(db.getItem(db.key(i))));
									}
								}
							}

						}

					}
				}

				var end = moment().valueOf();

				console.log("Take: " + (end - start) + "ms");

				return resultObj;
			};

			/*
			 * The result array's structure should be:
			 * 		[
			 *			{"_id" : projectId_1, "name" : projectName_1},
			 *			{"_id" : projectId_2, "name" : projectName_2},
			 *			...
			 *		]
			 */
			this.getAllProject = function getAllProject(username) {

				var resultArray = [];

				var valueStr =
					db.getItem(appConstants.projectStore.keyPrefix + appConstants.projectStore.keySeparator + username);

				var valueArray = [];

				if (valueStr != null && typeof(valueStr) != "undefined" && valueStr.length != 0) {
					valueArray = valueStr.split(appConstants.projectStore.valueSeparator);
				}

				for (var i = 0; i < valueArray.length; i++) {
					resultArray.push(JSON.parse(valueArray[i]));
				}

				return resultArray;

			};
			
		}
	]);

})();