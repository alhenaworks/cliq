(function() {
    TODO.service("TaskService", [
        "$rootScope", "$q", "socket", "DBService", "UserService", "SyncServerService", "ProjectService",
        function($rootScope, $q, socket, DBService, UserService, SyncServerService, ProjectService) {

            var self = this;

            this.updateTasksByLaneWithSingleTask = function updateTasksByLaneWithSingleTask(tasksByLane, task) {
                if (task.data.lane.length != 0) {
                    if (!tasksByLane.lanes.isContain(task.data.lane)) {
                        tasksByLane.lanes.push(task.data.lane);
                        tasksByLane[task.data.lane] = [
                            task
                        ];
                    } else {
                        tasksByLane[task.data.lane].push(task);
                    }
                }

                if (task.status == "d") {
                    tasksByLane["Done"].push(task);
                } else {
                    tasksByLane["TO DO"].push(task);

                    var dueDate = moment(task.data.dueDate);
                    var currentDate = moment();
                    var thisMonday = moment().weekday(1);
                    var thisSunday = moment().weekday(7);

                    if (dueDate.isAfter(thisMonday) && dueDate.isBefore(thisSunday)) {
                        tasksByLane["This Week"].push(task);
                        if (dueDate.isSame(currentDate, "day")) {
                            tasksByLane["Today"].push(task);
                        }
                    }
                }
            };

            this.buildLanesFully = function buildLanesFully(tasksByLane) {
                if (typeof(ProjectService.projectList) != "undefined") {
                    for (var item in ProjectService.projectList) {
                        if (!tasksByLane.lanes.isContain(item)) {
                            tasksByLane.lanes.push(item);
                        }
                        if (tasksByLane[item] == undefined) {
                            tasksByLane[item] = [];
                        }
                    }
                }                

                return tasksByLane;
            };

            this.buildTasksByLaneFromBroadcast = function buildTasksByLaneFromBroadcast(personalTasks) {

                var tasksByLane = {
                    "lanes" : ["TO DO", "This Week", "Today", "Done"],
                    "TO DO" : [],
                    "This Week" : [],
                    "Today" : [],
                    "Done" : []
                };

                for (var i = 0; i < personalTasks.length; i++) {
                    this.updateTasksByLaneWithSingleTask(tasksByLane, personalTasks[i]);
                }

                return this.buildLanesFully(tasksByLane);
            };

            this.buildTasksByLaneFromLocalStorage = function buildTasksByLaneFromLocalStorage(username) {
                return this.buildLanesFully(DBService.classifyAllTasksByProject(username));
            };

            this.isRepetitive = function isRepetitive(currentTODOTasks, task) {
                if (typeof(task) != "undefined" && task != null) {
                    for (var i = 0; i < currentTODOTasks.length; i++) {
                        if (currentTODOTasks[i]._id == task._id) {
                            return true;
                        }
                    }
                }                

                return false;
            };

            this.createTask = function createTask(data) {

                var deferred = $q.defer();

                if (!data || !data.title) {
                    deferred.reject("Adhoc task's title is undefined.");
                } else {

                    if (typeof(data.customerID) == "undefined" || data.customerID.length == 0) {
                        data.customerID = UserService.currentUser;
                    }

                    if (typeof(data.dueDate) == "undefined" || data.dueDate.length == 0) {
                        data.dueDate = moment().hour(23).minute(59).second(59).valueOf();
                    } else {
                        data.dueDate = moment(Number(data.dueDate)).hour(23).minute(59).second(59).valueOf();
                    }
                    
                    data.completeDate = "";

                    if (typeof(data.lane) == "undefined") {
                        data.lane = "";
                    }
                    
                    data.subTasks = [];

                    data.notes = "";
                    
                    socket.emit("task:action", {
                        type: "adhoc",
                        action: "create",
                        data: data
                    });

                    deferred.resolve(data);

                    socket.once("system:update", function(data) {

                        SyncServerService.retrieveTaskData();

                        $rootScope.$broadcast("task:create", data.action);
                    });

                    socket.once("system:error", function(data) {
                        console.log(data);
                    });

                }

                return deferred.promise;
            };

            this.updateTask = function updateTask(task) {

                var deferred = $q.defer();

                socket.emit("task:action", {
                    type: "adhoc",
                    action: "update",
                    task: task._id,
                    data: task,
                    project: ""
                });

                SyncServerService.retrieveTaskData();
                $rootScope.$broadcast("task:update", task);

                deferred.resolve(task);

                return deferred.promise;

            };

            this.buildTaskDayRelation = function buildTaskDayRelation(dayObj, taskObj, activeYear, activeMonth) {
                for (var i = 0; i < taskObj.length; i++) {
                    if (taskObj[i].status != "d") {
                        var tmpDate = moment(Number(taskObj[i].data.dueDate));
                        var dateArray = [tmpDate.year(), tmpDate.month() + 1, tmpDate.date()];
                        if (dateArray[0] == activeYear) {
                            if (dateArray[1] == activeMonth) {
                                for (var j = 0; j < dayObj.activeMonth.days.length; j++) {
                                    if (dayObj.activeMonth.days[j] == dateArray[2]) {
                                        dayObj.activeMonth.tasks[j].push(taskObj[i]);
                                    }
                                }
                            }
                        } 
                    }                  
                }
                
                return dayObj;
            };

            this.findTaskById = function findTaskById(id) {
                for (var i = 0; i < this.tasksByLane["TO DO"].length; i++) {
                    if (this.tasksByLane["TO DO"][i]._id == id) {
                        return this.tasksByLane["TO DO"][i];
                    }
                }

                return null;
            };

            /*---------------------------------I am also a cute divider---------------------------------*/

            this.getTasksAmountForToday = function getTasksAmountForToday() {
                var amount = 0;
                var currentDate = moment();
                for (var i = 0; i < this.tasksByLane["TO DO"].length; i++) {
                    var taskDueDate = moment(this.tasksByLane["TO DO"][i].data.dueDate);
                    if (taskDueDate.isSame(currentDate, "day")) {
                        amount++;
                    }
                }

                return amount;
            };

            this.getTaskListForToday = function getTaskListForToday() {
                var tasks = [];
                var currentDate = moment();
                for (var i = 0; i < this.tasksByLane["TO DO"].length; i++) {
                    var taskDueDate = moment(this.tasksByLane["TO DO"][i].data.dueDate);
                    if (taskDueDate.isSame(currentDate, "day")) {
                        tasks.push(this.tasksByLane["TO DO"][i]);
                    }
                }

                return tasks;
            };

            this.getMissedTaskList = function getMissedTaskList() {
                var tasks = [];
                var currentDate = moment();
                for (var i = 0; i < this.tasksByLane["TO DO"].length; i++) {
                    var taskDueDate = moment(this.tasksByLane["TO DO"][i].data.dueDate);
                    if (taskDueDate.isBefore(currentDate, "day")) {
                        tasks.push(this.tasksByLane["TO DO"][i]);
                    }
                }

                return tasks;
            };

        }
    ]);

})();