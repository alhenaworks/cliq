(function() {

    TODO.controller("TwoLaneTaskSummaryCtrl", [
        "$scope", "TaskService", "UserService",
        function($scope, TaskService, UserService) {
        
        	$scope.lanes = TaskService.tasksByLane.lanes;
            
        	$scope.activeLaneLeft = TaskService.tasksByLane[$scope.lanes[0]];
        	$scope.activeLaneRight = TaskService.tasksByLane[$scope.lanes[1]];

        	$scope.goToNextTwoLanes = function goToNextTwoLanes() {
        		$scope.lanes.push($scope.lanes.shift());
        		$scope.lanes.push($scope.lanes.shift());
        		$scope.activeLaneLeft = TaskService.tasksByLane[$scope.lanes[0]];
        		$scope.activeLaneRight = TaskService.tasksByLane[$scope.lanes[1]];
        	};

        	$scope.goToPrevTwoLanes = function goToPrevTwoLanes() {
        		$scope.lanes.unshift($scope.lanes.pop());
        		$scope.lanes.unshift($scope.lanes.pop());
        		$scope.activeLaneLeft = TaskService.tasksByLane[$scope.lanes[0]];
        		$scope.activeLaneRight = TaskService.tasksByLane[$scope.lanes[1]];
        	};

        }
    ]);

})();